#!/bin/bash

echo $#

if [ $# = 0 ] ; then
	echo exit mal
	exit 1
fi

export OUT=$1

echo "--------------------------------------------------" >> ${OUT}-gps.txt
(./gps_time >>  ${OUT}-gps.txt ) &
sleep 2

echo "--------------------------------------------------" >> ${OUT}-wifi.txt
date +%s >> ${OUT}-wifi.txt
iwlist scan >> ${OUT}-wifi.txt
date +%s >> ${OUT}-wifi.txt

sync

killall gps_time

