
Convertir de GPS data a UTM
---------------------------

Ej; de -38.3245 -68.2131 a UTM


https://epsg.io/4326
EPSG:4326
WGS 84 -- WGS84 - World Geodetic System 1984, used in GPS
Transform Get position on a map


https://epsg.io/22185
EPSG:22185
POSGAR 94 / Argentina 5


El GPS nos devuelve esto:
$GNRMC,144815.00,A,3849.79990,S,06804.08262,W,0.281,,030721,,,A*6C

En negativo y dos decimales transformamos a utm
echo -38.4979 -68.0408 | gdaltransform -s_srs epsg:4326 -t_srs epsg:22185  
